# User Search
A basic web app to search users

## Search Functionality
![Search](./gif/search.gif)

## Edit Details
![Edit](./gif/edit.gif)
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```
### Create test coverage report
```
yarn test:coverage
```

### Lints and fixes files
```
yarn lint
```
### Current test converage report

| File                     |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered Line #s |
| :---:                    | :---:    | :---:    | :---:    | :---:    | :---:             |
|All files                 |    96.32 |    57.14 |      100 |    96.12 |                   |
| components               |    98.25 |    66.67 |      100 |    98.15 |                   |
|  DetailsCard.vue         |      100 |      100 |      100 |      100 |                   |
|  ListItem.vue            |      100 |      100 |      100 |      100 |                   |
|  Search.vue              |    96.77 |    66.67 |      100 |    96.67 |               164 |
|  TypeAhead.vue           |      100 |      100 |      100 |      100 |                   |
| views                    |    94.94 |       50 |      100 |    94.67 |                   |
|  Home.vue                |    94.74 |       50 |      100 |    94.44 |                61 |
|  OrganizationDetails.vue |    94.44 |       50 |      100 |    94.12 |                58 |
|  TicketDetails.vue       |    94.44 |       50 |      100 |    94.12 |                77 |
|  UserDetails.vue         |    95.83 |       50 |      100 |    95.65 |               130 |
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
