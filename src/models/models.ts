/* eslint-disable camelcase */
export interface Organization {
  _id?: number;
  url?: string;
  external_id?: string;
  name?: string;
  domain_names?: (string)[] | null;
  created_at?: string;
  details?: string;
  shared_tickets?: boolean;
  tags?: (string)[] | null;
}

export interface Ticket {
  _id?: string;
  url?: string;
  external_id?: string;
  created_at?: string;
  type?: string;
  subject?: string;
  description?: string;
  priority?: string;
  status?: string;
  submitter_id?: number;
  assignee_id?: number;
  organization_id?: number;
  tags?: (string)[] | null;
  has_incidents?: boolean;
  due_at?: string;
  via?: string;
}

export interface User {
  _id: number;
  url?: string;
  external_id?: string;
  name?: string;
  alias?: string;
  created_at?: string;
  active?: boolean;
  verified?: boolean;
  shared?: boolean;
  locale?: string;
  timezone?: string;
  last_login_at?: string;
  email?: string;
  phone?: string;
  signature?: string;
  organization_id?: number;
  tags?: (string)[] | null;
  suspended?: boolean;
  role?: string;
  matched_field?: string;
}

export interface UserState {
  users: Array<User>
}

export interface UserSearchSuggestion {
  matchedPropValue?: string;
  matchedProp: string;
  id?: number;
}

export interface EditableObject {
  label: keyof User | keyof Organization | keyof Ticket;
  value?: string | number;
  isEditable: boolean;
}
