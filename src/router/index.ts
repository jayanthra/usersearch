import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/user/:id',
    name: 'userdetails',
    // route level code-splitting
    // this generates a separate chunk (details.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "details" */ '../views/UserDetails.vue')
  },
  {
    path: '/ticket/:id',
    name: 'ticketdetails',
    component: () => import(/* webpackChunkName: "details" */ '../views/TicketDetails.vue')
  },
  {
    path: '/organization/:id',
    name: 'organizationdetails',
    component: () => import(/* webpackChunkName: "details" */ '../views/OrganizationDetails.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
