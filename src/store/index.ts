import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import ticket from './modules/ticket'
import organization from './modules/organization'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    user,
    ticket,
    organization
  }
})
