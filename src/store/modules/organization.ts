import { Organization } from '@/models/models'
import organizationJSON from '@/db/organizations.json'

interface OrganizationState {
  organizations: Array<Organization>
}

const state: OrganizationState = {
  organizations: []
}

export default {
  state,
  mutations: {
    loadAllOrganizations (state: OrganizationState): void {
      state.organizations = organizationJSON
    },
    updateOrganization (state: OrganizationState, editedOrganization: Organization): void {
      state.organizations = [
        ...state.organizations.filter(element => element._id !== editedOrganization._id),
        editedOrganization
      ]
    }
  },
  actions: {
    loadAllOrganizations (context: any): void {
      context.commit('loadAllOrganizations')
    },
    updateOrganization (context: any): void {
      context.commit('updateOrganization')
    }
  },
  getters: {
    getOrganizationById: (state: OrganizationState) => (id: number): Organization => {
      return state.organizations.filter((oraganization: Organization) => oraganization._id?.toString() === id.toString())[0]
    }
  }
}
