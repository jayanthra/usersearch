import { Ticket } from '@/models/models'
import ticketsJSON from '@/db/tickets.json'

interface TicketState {
  tickets: Array<Ticket>
}

const state: TicketState = {
  tickets: []
}

export default {
  state,
  mutations: {
    loadAllTickets (state: TicketState) : void {
      state.tickets = ticketsJSON
    },
    updateTicket (state: TicketState, editedTicket: Ticket): void {
      state.tickets = [
        ...state.tickets.filter(element => element._id !== editedTicket._id),
        editedTicket
      ]
    }
  },
  actions: {
    loadAllTickets (context: any) : void {
      context.commit('loadAllTickets')
    },
    updateTicket (context: any): void {
      context.commit('updateTicket')
    }
  },
  getters: {
    filterTickets: (state: TicketState) => (id: number) : Array<Ticket> => {
      return state.tickets.filter((ticket : Ticket) => ticket.submitter_id === id)
    },
    getTicketById: (state: TicketState) => (id: number): Ticket => {
      return state.tickets.filter((ticket: Ticket) => ticket._id?.toString() === id.toString())[0]
    }
  }
}
