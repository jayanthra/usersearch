import { User, UserState, UserSearchSuggestion } from '@/models/models'
import usersJSON from '@/db/users.json'

const state: UserState = {
  users: []
}

export default {
  state,
  mutations: {
    loadAllUsers (state: UserState): void {
      state.users = usersJSON
    },
    updateUser (state: UserState, editedUser: User): void {
      state.users = [
        ...state.users.filter(element => element._id !== editedUser._id),
        editedUser
      ]
    }
  },
  actions: {
    loadAllUsers (context: any): void {
      context.commit('loadAllUsers')
    },
    updateUser (context: any): void {
      context.commit('updateUser')
    }
  },
  getters: {
    filterUsers: (state: UserState) => (searchTerm: string, propsList: (keyof User)[]): Array<User> => {
      const filterUserList: Array<User> = []

      state.users.filter((user: User) => {
        propsList.forEach((prop: keyof User) => {
          if (user[prop]?.toString().toLowerCase().includes(searchTerm.toLowerCase())) {
            if (!filterUserList.some(e => e._id === user._id)) {
              user.matched_field = prop
              filterUserList.push(user)
            }
          }
        })

        const matchedString = user.tags?.find((item: string) => item.toLowerCase().includes(searchTerm.toLowerCase()))
        if (matchedString) {
          if (!filterUserList.some(e => e._id === user._id)) {
            user.matched_field = 'Tags'
            filterUserList.push(user)
          }
        }
      })

      return filterUserList
    },
    userListLength: (state: UserState): number => {
      return state.users.length
    },
    getUserById: (state: UserState) => (id: string): User | undefined => {
      return state.users.find(({ _id }) => _id?.toString() === id)
    },
    searchSuggestions: (state: UserState) => (searchTerm: string, filterProps: (keyof User)[]): Array<UserSearchSuggestion> => {
      const suggestions: Array<UserSearchSuggestion> = []
      for (let i = 0; i < state.users.length; i++) {
        const user = state.users[i]
        let found = false
        const propsList: (keyof User)[] = filterProps
        propsList.forEach(key => {
          if (user[key]?.toString().toLowerCase().includes(searchTerm.toLowerCase())) {
            // avoid duplicates
            if (!suggestions.some(suggestion => suggestion.id === user._id) &&
            !suggestions.some(suggestion => suggestion.matchedProp === key)) {
              suggestions.push({
                matchedPropValue: user[key]?.toString(),
                matchedProp: key,
                id: user._id
              })
              found = true
            }
          }
        })

        // not found in props specified, search in tags
        if (!found) {
          const matchedString = user.tags?.find((item: string) => item.toLowerCase().includes(searchTerm.toLowerCase()))
          if (matchedString) {
            suggestions.push({
              matchedPropValue: matchedString,
              matchedProp: 'tags',
              id: user._id
            })
          }
        }

        // stop searching after 10 matches
        if (suggestions.length === 10) {
          break
        }
      }

      console.log('sugges', suggestions)
      return suggestions
    }
  }
}
