export const debounce = <F extends (...args: any) => any>(func: F, delay: number) => {
  let timeout = 0

  const debounced = (...args: any) => {
    clearTimeout(timeout)
    timeout = setTimeout(() => func(...args), delay)
  }

  return debounced as (...args: Parameters<F>) => ReturnType<F>
}
