import Vue from 'vue'

export class GlobalMixin extends Vue {
  async initializeStore (): Promise<void> {
    await this.$store.commit('loadAllUsers')
    await this.$store.commit('loadAllTickets')
    await this.$store.commit('loadAllOrganizations')
  }
}
