module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  collectCoverage: true,
  collectCoverageFrom: [
    'components',
    'Utilts',
    'views'
  ].map(subdir => `src/${subdir}/**/*.(vue|ts|js)`)
}
