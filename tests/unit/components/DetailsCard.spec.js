import { shallowMount } from '@vue/test-utils'
import DetailsCard from '@/components/DetailsCard.vue'

describe('DetailsCard.vue', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallowMount(DetailsCard, {
      propsData: {
        detailsFiltered: [
          {
            label: 'name',
            value: 'Multron',
            isEditable: true
          },
          {
            label: '_id',
            value: 119,
            isEditable: false
          },
          {
            label: 'details',
            value: 'Non profit',
            isEditable: true
          }
        ]
      }
    })
  })

  describe('DetailsCard component UI elements', () => {
    it('renders default view mode UI elements', () => {
      expect(wrapper.find('.details-card').exists())
      expect(wrapper.find('.title').exists())
      expect(wrapper.find('.view-mode').exists())
      expect(wrapper.find('.edit-btn').exists())
      expect(wrapper.vm.isEditMode).toBe(false)
    })

    it('renders edit mode UI elements', async () => {
      wrapper.vm.enterEditMode = jest.fn()
      const editBtn = wrapper.find('.edit-btn')
      await editBtn.trigger('click')
      expect(wrapper.find('.edit-mode').exists())
      expect(wrapper.find('.btn-group').exists())
      expect(wrapper.vm.isEditMode).toBe(true)
    })
  })

  describe('DetailsCard component methods', () => {
    it('enters edit mode', async () => {
      const editBtn = wrapper.find('.edit-btn')
      await editBtn.trigger('click')
      expect(wrapper.vm.isEditMode).toBe(true)
      expect(wrapper.vm.tempEditableObject).toStrictEqual(wrapper.vm.tempEditableObject)
    })

    it('exits edit mode', async () => {
      const editBtn = wrapper.find('.edit-btn')
      await editBtn.trigger('click')

      const cancelBtn = wrapper.find('.cancel-btn')
      await cancelBtn.trigger('click')

      expect(wrapper.vm.isEditMode).toBe(false)
      expect(wrapper.vm.tempEditableObject).toStrictEqual(wrapper.vm.tempEditableObject)
    })

    it('emits event on update click', async () => {
      const editBtn = wrapper.find('.edit-btn')
      await editBtn.trigger('click')

      const updateBtn = wrapper.find('.update-btn')
      await updateBtn.trigger('click')

      expect(wrapper.vm.isEditMode).toBe(false)
      expect(wrapper.emitted().detailsupdated).toBeTruthy()
    })
  })
})
