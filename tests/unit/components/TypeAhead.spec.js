import { shallowMount } from '@vue/test-utils'
import TypeAhead from '@/components/TypeAhead.vue'

describe('TypeAhead.vue', () => {
  let wrapper
  beforeAll(() => {
    wrapper = shallowMount(TypeAhead, {
      propsData: {
        typedString: 'adm',
        matchedPropValue: 'admin',
        matchedProp: 'role'
      }
    })
  })

  describe('TypeAhead component UI elements', () => {
    it('renders UI elements', () => {
      expect(wrapper.find('.type-ahead').exists())
      expect(wrapper.find('.tag').exists())
      expect(wrapper.find('.value').exists())

      const matchedValue = wrapper.find('.value')
      expect(matchedValue.text()).toBe('admin')

      const matchedProp = wrapper.find('.tag')
      expect(matchedProp.text()).toBe('ROLE')
    })
  })

  describe('TypeAhead methods', () => {
    it('emit event to parent on click of text', async () => {
      const matchedText = wrapper.find('.type-ahead')
      await matchedText.trigger('click')
      expect(wrapper.emitted().suggestionclicked).toBeTruthy()
      expect(wrapper.emitted().suggestionclicked.length).toBe(1)
      expect(wrapper.emitted().suggestionclicked[0][0]).toBe('admin')
    })
  })
})
