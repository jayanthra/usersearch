import { shallowMount, createLocalVue } from '@vue/test-utils'
import Search from '@/components/Search.vue'
import Vuex from 'vuex'
import { mockstore } from '../../mocks/mockstore'
import { mockSuggestions } from '../../mocks/mockdata'

const localVue = createLocalVue()
localVue.use(Vuex)
describe('Search.vue', () => {
  describe('Search component UI elements', () => {
    let wrapper
    beforeAll(() => {
      wrapper = shallowMount(Search)
    })

    it('renders filter drop down list', () => {
      expect(wrapper.find('.dropdown-list').exists())
    })

    it('renders filter drop down button', () => {
      expect(wrapper.find('.dropdown-btn').exists())
    })

    it('renders search box', () => {
      expect(wrapper.find('.search-box').exists())
    })

    it('renders search button', () => {
      expect(wrapper.find('.search-btn').exists())
    })

    it('toggles filter selection list', async () => {
      expect(wrapper.vm.isDropdownVisible).toBe(false)
      await wrapper.find('.dropdown-btn').trigger('click')
      expect(wrapper.vm.isDropdownVisible).toBe(true)
    })

    it('renders search button as disabled', () => {
      const btn = wrapper.find('.search-btn')
      expect(btn.attributes('disabled')).toBe('disabled')
    })

    it('enable search button on text entered', async () => {
      const btn = wrapper.find('.search-btn')
      wrapper.setData({ searchKey: 'All' })
      await wrapper.find('.search-box').trigger('keyup', {
        key: 'A'
      })
      expect(btn.attributes('disabled')).toBe(undefined)
    })
  })

  describe('Search component methods', () => {
    let wrapper

    beforeEach(() => {
      const store = new Vuex.Store(mockstore)
      wrapper = shallowMount(Search, { localVue, store })
      jest.useFakeTimers()
    })
    it('calls search', async () => {
      wrapper.vm.searchUser = jest.fn()
      wrapper.setData({ searchKey: 'All' })
      await wrapper.find('.search-box').trigger('keydown', {
        key: 'A'
      })
      await wrapper.find('.search-btn').trigger('click')
      expect(wrapper.vm.isError).toBe(false)
      expect(wrapper.vm.searchUser).toBeCalled()
    })

    it('emits event to parent from search method', async () => {
      wrapper.vm.searchKey = 'test'
      wrapper.vm.searchUser()
      expect(wrapper.vm.isDropdownVisible).toBeFalsy()
      expect(wrapper.vm.isError).toBeFalsy()
      expect(wrapper.vm.suggestionList).toStrictEqual([])

      expect(wrapper.emitted().searchuser).toBeTruthy()
      expect(wrapper.emitted().searchuser[0].length).toBe(2)
      expect(wrapper.emitted().searchuser[0][0]).toBe('test')
    })

    it('shows error if search clicked when no filter selected', async () => {
      wrapper.vm.filterOptions.forEach(item => {
        item.isSelected = false
      })
      wrapper.vm.propsList = []
      wrapper.setData({ searchKey: 'All' })
      await wrapper.find('.search-box').trigger('keydown', {
        key: 'A'
      })
      await wrapper.find('.search-btn').trigger('click')
      expect(wrapper.vm.isError).toBe(true)
      expect(wrapper.find('.error').exists())
    })

    it('loads suggestion data', async () => {
      wrapper.vm.getSuggestions = jest.fn()
      wrapper.setData({ searchKey: 'All' })
      await wrapper.find('.search-box').trigger('keyup', {
        key: 'X'
      })
      jest.advanceTimersByTime(1000)
      expect(wrapper.vm.$store.getters.searchSuggestions()).toStrictEqual(mockSuggestions)
      expect(wrapper.vm.suggestionList.length).toBe(9)
    })

    it('emits event to parent from suggestion', async () => {
      wrapper.vm.suggestionClicked('test')
      expect(wrapper.emitted().searchuser).toBeTruthy()
      expect(wrapper.emitted().searchuser[0].length).toBe(2)
      expect(wrapper.emitted().searchuser[0][0]).toBe('test')
    })

    afterAll(() => {
      jest.clearAllTimers()
    })
  })
})
