import { createLocalVue, RouterLinkStub, shallowMount } from '@vue/test-utils'
import UserDetails from '@/views/UserDetails.vue'
import Vuex from 'vuex'
import { mockstore } from '../../mocks/mockstore'
import { mockUsers, mockTickets, mockOrganizations, mockUpdateUser } from '../../mocks/mockdata'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('UserDetails.vue', () => {
  let wrapper
  beforeAll(() => {
    const store = new Vuex.Store(mockstore)
    wrapper = shallowMount(UserDetails, {
      localVue,
      store,
      stubs: {
        RouterLink: RouterLinkStub
      },
      mocks: {
        $route: {
          params: {
            id: '2'
          }
        }
      }
    })
    wrapper.vm.initializeStore = jest.fn()
  })
  describe('UserDetails page UI elements', () => {
    it('renders three sections for details', () => {
      const sections = wrapper.findAll('section')
      expect(sections.length).toBe(3)
    })

    it('renders titles', () => {
      const titles = wrapper.findAll('h2')
      expect(titles.length).toBe(2)
      expect(titles.at(0).text()).toBe('Organization')
      expect(titles.at(1).text()).toBe('Tickets')
    })

    it('renders details', () => {
      const details = wrapper.findAll('.details')
      // 1 detail of organization
      // 4 tickets
      expect(details.length).toBe(5)
    })
  })

  describe('UserDetails methods', () => {
    it('loads user details', async () => {
      await wrapper.vm.loadUser()
      expect(wrapper.vm.userDetails).toBe(mockUsers[0])
    })
    it('loads ticket details', async () => {
      await wrapper.vm.loadTickets()
      expect(wrapper.vm.ticketDetails.length).toBe(4)
      expect(wrapper.vm.ticketDetails).toBe(mockTickets)
    })

    it('loads organization details', async () => {
      await wrapper.vm.loadOrganization()
      expect(wrapper.vm.organizationDetails).toBe(mockOrganizations[0])
    })

    it('accepts edited data from details card', () => {
      wrapper.vm.createDetailsList = jest.fn()
      expect(wrapper.vm.userDetails).toStrictEqual(mockUsers[0])
      const mockAttrs = [
        {
          label: 'name',
          value: 'New Updated',
          isEditable: true
        },
        {
          label: '_id',
          value: 1,
          isEditable: false
        },
        {
          label: 'alias',
          value: 'Mock Alias',
          isEditable: true
        },
        {
          label: 'role',
          value: 'admin',
          isEditable: false
        },
        {
          label: 'email',
          value: 'test@mock.com',
          isEditable: true
        },
        {
          label: 'phone',
          value: '1111-111-1111',
          isEditable: true
        },
        {
          label: 'signature',
          value: 'New Mock Signature',
          isEditable: true
        }
      ]

      wrapper.vm.updateUser(mockAttrs)
      expect(wrapper.vm.userDetails).toStrictEqual(mockUpdateUser)
      expect(wrapper.vm.createDetailsList).toBeCalled()
    })
  })
})
