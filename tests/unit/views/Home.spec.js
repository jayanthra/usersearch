import { shallowMount, createLocalVue } from '@vue/test-utils'
import Home from '@/views/Home.vue'
import Vuex from 'vuex'
import { mockstore } from '../../mocks/mockstore'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Home.vue', () => {
  let wrapper
  beforeAll(() => {
    const store = new Vuex.Store(mockstore)
    wrapper = shallowMount(Home, { localVue, store })
    wrapper.vm.initializeStore = jest.fn()
  })
  describe('Home page UI elements', () => {
    it('renders search sticky bar', () => {
      expect(wrapper.find('.search-sticky').exists())
    })

    it('renders user list container', () => {
      expect(wrapper.find('.user-list').exists())
    })
  })

  describe('User search list', () => {
    it('Loads master list', () => {
      wrapper.vm.searchUser('test', ['_id'])
      expect(wrapper.vm.masterUserList.length).toBe(10)
    })
  })

  describe('Pagination buttons', () => {
    it('pagination functionality', async () => {
      await wrapper.vm.searchUser('test', ['_id'])
      const prevButton = wrapper.find('.prev')
      const nextButton = wrapper.find('.next')

      expect(wrapper.vm.pageNumber).toBe(0)
      expect(prevButton.attributes('disabled')).toBe('disabled')
      expect(nextButton.attributes('disabled')).toBe(undefined)
      expect(wrapper.vm.paginatedUserList.length).toBe(8)

      await nextButton.trigger('click')
      expect(wrapper.vm.pageNumber).toBe(1)
      expect(nextButton.attributes('disabled')).toBe('disabled')
      expect(prevButton.attributes('disabled')).toBe(undefined)
      expect(wrapper.vm.paginatedUserList.length).toBe(2)
    })
  })
})
