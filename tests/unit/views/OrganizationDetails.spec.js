import { createLocalVue, RouterLinkStub, shallowMount } from '@vue/test-utils'
import OrganizationDetails from '@/views/OrganizationDetails.vue'
import Vuex from 'vuex'
import { mockstore } from '../../mocks/mockstore'
import { mockOrganizations, mockUpdatedOrganization } from '../../mocks/mockdata'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('OrganizationDetails.vue', () => {
  let wrapper
  beforeAll(() => {
    const store = new Vuex.Store(mockstore)
    wrapper = shallowMount(OrganizationDetails, {
      localVue,
      store,
      stubs: {
        RouterLink: RouterLinkStub
      },
      mocks: {
        $route: {
          params: {
            id: '2'
          }
        }
      }
    })
    wrapper.vm.initializeStore = jest.fn()
  })
  describe('OrganizationDetails page UI elements', () => {
    it('renders section for details', () => {
      const sections = wrapper.findAll('section')
      expect(sections.length).toBe(1)
    })

    it('renders title', () => {
      const titles = wrapper.find('h2')
      expect(titles.text()).toBe('Organization')
    })
  })

  describe('OrganizationDetails methods', () => {
    it('loads organization details', async () => {
      await wrapper.vm.loadOrganization()
      expect(wrapper.vm.organizationDetails).toBe(mockOrganizations[0])
    })
  })

  describe('OrganizationDetails methods', () => {
    it('loads organization details', async () => {
      await wrapper.vm.loadOrganization()
      expect(wrapper.vm.organizationDetails).toBe(mockOrganizations[0])
    })

    it('accepts edited data from details card', () => {
      wrapper.vm.createDetailsList = jest.fn()
      expect(wrapper.vm.organizationDetails).toStrictEqual(mockOrganizations[0])
      const mockAttrs = [
        {
          label: 'name',
          value: 'Sample',
          isEditable: true
        },
        {
          label: '_id',
          value: 101,
          isEditable: false
        },
        {
          label: 'details',
          value: 'For profit',
          isEditable: true
        }
      ]

      wrapper.vm.updateOrganization(mockAttrs)
      expect(wrapper.vm.organizationDetails).toStrictEqual(mockUpdatedOrganization)
      expect(wrapper.vm.createDetailsList).toBeCalled()
    })
  })
})
