import { createLocalVue, RouterLinkStub, shallowMount } from '@vue/test-utils'
import TicketDetails from '@/views/TicketDetails.vue'
import Vuex from 'vuex'
import { mockstore } from '../../mocks/mockstore'
import { mockTickets, mockUpdateTicket } from '../../mocks/mockdata'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('TicketDetails.vue', () => {
  let wrapper
  beforeAll(() => {
    const store = new Vuex.Store(mockstore)
    wrapper = shallowMount(TicketDetails, {
      localVue,
      store,
      stubs: {
        RouterLink: RouterLinkStub
      },
      mocks: {
        $route: {
          params: {
            id: '2'
          }
        }
      }
    })
    wrapper.vm.initializeStore = jest.fn()
  })
  describe('TicketDetails page UI elements', () => {
    it('renders section for details', () => {
      const sections = wrapper.findAll('section')
      expect(sections.length).toBe(1)
    })

    it('renders title', () => {
      const titles = wrapper.find('h2')
      expect(titles.text()).toBe('Ticket')
    })
  })

  describe('TicketDetails methods', () => {
    it('loads ticket details', async () => {
      await wrapper.vm.loadTicket()
      expect(wrapper.vm.ticketDetails).toBe(mockTickets[0])
    })

    it('accepts edited data from details card', () => {
      wrapper.vm.createDetailsList = jest.fn()
      expect(wrapper.vm.ticketDetails).toStrictEqual(mockTickets[0])
      const mockAttrs = [
        {
          label: 'type',
          value: 'problem',
          isEditable: true
        },
        {
          label: '_id',
          value: '436bf9b0-1147-4c0a-8439-6f79833bff5b',
          isEditable: false
        },
        {
          label: 'subject',
          value: 'The updated problem',
          isEditable: true
        },
        {
          label: 'description',
          value: 'Nostrud ad sit',
          isEditable: true
        },
        {
          label: 'priority',
          value: 'low',
          isEditable: true
        },
        {
          label: 'status',
          value: 'pending',
          isEditable: false
        },
        {
          label: 'via',
          value: 'web',
          isEditable: false
        }
      ]
      wrapper.vm.updateTicket(mockAttrs)
      expect(wrapper.vm.ticketDetails).toStrictEqual(mockUpdateTicket)
      expect(wrapper.vm.createDetailsList).toBeCalled()
    })
  })
})
