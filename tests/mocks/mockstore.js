import { mockUsers, mockTickets, mockOrganizations, mockSuggestions } from './mockdata'

export const mockstore = {
  state: {
    users: mockUsers,
    tickets: mockTickets,
    organizations: mockOrganizations
  },
  mutations: {
    loadAllUsers (state) {
      state.users = mockUsers
    },
    loadAllTickets (state) {
      state.tickets = mockTickets
    },
    loadAllOrganizations (state) {
      state.organizations = mockOrganizations
    },
    updateOrganization (state, updated) {
      state.organizations = [
        ...state.organizations.filter(element => element._id !== updated._id),
        updated
      ]
    },
    updateTicket (state, updated) {
      state.tickets = [
        ...state.tickets.filter(element => element._id !== updated._id),
        updated
      ]
    },
    updateUser (state, updated) {
      state.tickets = [
        ...state.users.filter(element => element._id !== updated._id),
        updated
      ]
    }
  },
  getters: {
    filterUsers (state) {
      return (id) => {
        return mockUsers
      }
    },
    userListLength (state) {
      return state.users.length
    },
    searchSuggestions (state) {
      return (id) => {
        return mockSuggestions
      }
    },
    getUserById (state) {
      return (id) => {
        return mockUsers[0]
      }
    },
    getOrganizationById () {
      return (id) => {
        return mockOrganizations[0]
      }
    },
    getTicketById () {
      return (id) => {
        return mockTickets[0]
      }
    },
    filterTickets () {
      return (id) => {
        return mockTickets
      }
    }
  }
}
