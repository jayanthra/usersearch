export const mockUsers = [
  {
    _id: 1,
    url: 'http://initech.tokoin.io.com/api/v2/users/1.json',
    external_id: '74341f74-9c79-49d5-9611-87ef9b6eb75f',
    name: 'Francisca Rasmussen',
    alias: 'Miss Coffey',
    created_at: '2016-04-15T05:19:46 -10:00',
    active: true,
    verified: true,
    shared: false,
    locale: 'en-AU',
    timezone: 'Sri Lanka',
    last_login_at: '2013-08-04T01:03:27 -10:00',
    email: 'coffeyrasmussen@flotonic.com',
    phone: '8335-422-718',
    signature: "Don't Worry Be Happy!",
    organization_id: 119,
    tags: [
      'Springville',
      'Sutton',
      'Hartsville/Hartley',
      'Diaperville',
      'Henrietta'
    ],
    suspended: true,
    role: 'admin'
  },
  {
    _id: 2,
    url: 'http://initech.tokoin.io.com/api/v2/users/2.json',
    external_id: 'c9995ea4-ff72-46e0-ab77-dfe0ae1ef6c2',
    name: 'Cross Barlow',
    alias: 'Miss Joni',
    created_at: '2016-06-23T10:31:39 -10:00',
    active: true,
    verified: true,
    shared: false,
    locale: 'zh-CN',
    timezone: 'Armenia',
    last_login_at: '2012-04-12T04:03:28 -10:00',
    email: 'jonibarlow@flotonic.com',
    phone: '9575-552-585',
    signature: "Don't Worry Be Happy!",
    organization_id: 106,
    tags: [
      'Foxworth',
      'Woodlands',
      'Herlong',
      'Henrietta'
    ],
    suspended: false,
    role: 'admin'
  },
  {
    _id: 3,
    url: 'http://initech.tokoin.io.com/api/v2/users/3.json',
    external_id: '85c599c1-ebab-474d-a4e6-32f1c06e8730',
    name: 'Ingrid Wagner',
    alias: 'Miss Buck',
    created_at: '2016-07-28T05:29:25 -10:00',
    active: false,
    verified: false,
    shared: false,
    locale: 'en-AU',
    timezone: 'Trinidad and Tobago',
    last_login_at: '2013-02-07T05:53:38 -11:00',
    email: 'buckwagner@flotonic.com',
    phone: '9365-482-943',
    signature: "Don't Worry Be Happy!",
    organization_id: 104,
    tags: [
      'Mulino',
      'Kenwood',
      'Wescosville',
      'Loyalhanna'
    ],
    suspended: false,
    role: 'end-user'
  },
  {
    _id: 4,
    url: 'http://initech.tokoin.io.com/api/v2/users/4.json',
    external_id: '37c9aef5-cf01-4b07-af24-c6c49ac1d1c7',
    name: 'Rose Newton',
    alias: 'Mr Cardenas',
    created_at: '2016-02-09T07:52:10 -11:00',
    active: true,
    verified: true,
    shared: true,
    locale: 'de-CH',
    timezone: 'Netherlands',
    last_login_at: '2012-09-25T01:32:46 -10:00',
    email: 'cardenasnewton@flotonic.com',
    phone: '8685-482-450',
    signature: "Don't Worry Be Happy!",
    organization_id: 122,
    tags: [
      'Gallina',
      'Glenshaw',
      'Rowe',
      'Babb'
    ],
    suspended: true,
    role: 'end-user'
  },
  {
    _id: 5,
    url: 'http://initech.tokoin.io.com/api/v2/users/5.json',
    external_id: '29c18801-fb42-433d-8674-f37d63e637df',
    name: 'Loraine Pittman',
    alias: 'Mr Ola',
    created_at: '2016-06-12T08:49:19 -10:00',
    active: true,
    verified: false,
    shared: false,
    locale: 'zh-CN',
    timezone: 'Monaco',
    last_login_at: '2013-07-03T06:59:27 -10:00',
    email: 'olapittman@flotonic.com',
    phone: '9805-292-618',
    signature: "Don't Worry Be Happy!",
    organization_id: 101,
    tags: [
      'Frizzleburg',
      'Forestburg',
      'Sandston',
      'Delco'
    ],
    suspended: false,
    role: 'admin'
  },
  {
    _id: 6,
    url: 'http://initech.tokoin.io.com/api/v2/users/6.json',
    external_id: 'ed106e63-396d-4d16-ae49-d3dd37049ba3',
    name: 'Riggs Hebert',
    alias: 'Miss Shelton',
    created_at: '2016-04-04T01:30:49 -10:00',
    active: false,
    verified: false,
    shared: false,
    locale: 'zh-CN',
    timezone: 'Liberia',
    last_login_at: '2012-08-24T10:20:58 -10:00',
    email: 'sheltonhebert@flotonic.com',
    phone: '8935-413-112',
    signature: "Don't Worry Be Happy!",
    organization_id: 109,
    tags: [
      'Belfair',
      'Chamberino',
      'Roberts',
      'Cascades'
    ],
    suspended: false,
    role: 'end-user'
  },
  {
    _id: 7,
    url: 'http://initech.tokoin.io.com/api/v2/users/7.json',
    external_id: 'bce94e82-b4f4-438f-bc0b-2440e8265705',
    name: 'Lou Schmidt',
    alias: 'Miss Shannon',
    created_at: '2016-05-07T08:43:52 -10:00',
    active: false,
    verified: false,
    shared: false,
    locale: 'en-AU',
    timezone: 'Central African Republic',
    last_login_at: '2016-02-25T12:26:31 -11:00',
    email: 'shannonschmidt@flotonic.com',
    phone: '9094-083-167',
    signature: "Don't Worry Be Happy!",
    organization_id: 104,
    tags: [
      'Cawood',
      'Disautel',
      'Boling',
      'Southview'
    ],
    suspended: true,
    role: 'admin'
  },
  {
    _id: 8,
    url: 'http://initech.tokoin.io.com/api/v2/users/8.json',
    external_id: 'fa13ffa4-0ba1-41d1-be4a-c1e7a92f25e4',
    name: 'Lolita Herring',
    alias: 'Miss Reyna',
    created_at: '2016-07-14T03:21:49 -10:00',
    active: false,
    verified: false,
    shared: false,
    locale: 'de-CH',
    timezone: 'Iceland',
    last_login_at: '2014-10-28T03:06:58 -11:00',
    email: 'reynaherring@flotonic.com',
    phone: '8484-692-871',
    signature: "Don't Worry Be Happy!",
    organization_id: 125,
    tags: [
      'Greenbush',
      'Canby',
      'Bedias',
      'Boyd'
    ],
    suspended: true,
    role: 'agent'
  },
  {
    _id: 9,
    url: 'http://initech.tokoin.io.com/api/v2/users/9.json',
    external_id: '215e9cb6-cd30-444d-95f3-4c172d68859d',
    name: 'Josefa Mcfadden',
    alias: 'Mr Casey',
    created_at: '2016-03-26T04:11:05 -11:00',
    active: true,
    verified: false,
    shared: false,
    locale: 'zh-CN',
    timezone: 'France, Metropolitan',
    last_login_at: '2013-04-25T09:04:57 -10:00',
    email: 'caseymcfadden@flotonic.com',
    phone: '8715-273-472',
    signature: "Don't Worry Be Happy!",
    organization_id: 123,
    tags: [
      'Fresno',
      'Shawmut',
      'Buxton',
      'Winchester'
    ],
    suspended: true,
    role: 'agent'
  },
  {
    _id: 10,
    url: 'http://initech.tokoin.io.com/api/v2/users/10.json',
    external_id: 'f744706e-df3d-4d51-ad18-c63e41be5cc0',
    name: 'Kari Vinson',
    alias: 'Mr Webb',
    created_at: '2016-02-08T04:32:38 -11:00',
    active: true,
    verified: false,
    shared: true,
    locale: 'en-AU',
    timezone: 'Germany',
    last_login_at: '2014-12-26T04:54:30 -11:00',
    email: 'webbvinson@flotonic.com',
    phone: '8235-883-140',
    signature: "Don't Worry Be Happy!",
    organization_id: 105,
    tags: [
      'Moraida',
      'Beechmont',
      'Jeff',
      'Starks'
    ],
    suspended: false,
    role: 'end-user'
  }
]

export const mockTickets = [
  {
    _id: '436bf9b0-1147-4c0a-8439-6f79833bff5b',
    url: 'http://initech.tokoin.io.com/api/v2/tickets/436bf9b0-1147-4c0a-8439-6f79833bff5b.json',
    external_id: '9210cdc9-4bee-485f-a078-35396cd74063',
    created_at: '2016-04-28T11:19:34 -10:00',
    type: 'incident',
    subject: 'A Catastrophe in Korea (North)',
    description: 'Nostrud ad sit velit cupidatat laboris ipsum nisi amet laboris ex exercitation amet et proident. Ipsum fugiat aute dolore tempor nostrud velit ipsum.',
    priority: 'high',
    status: 'pending',
    submitter_id: 38,
    assignee_id: 24,
    organization_id: 116,
    tags: [
      'Ohio',
      'Pennsylvania',
      'American Samoa',
      'Northern Mariana Islands'
    ],
    has_incidents: false,
    due_at: '2016-07-31T02:37:50 -10:00',
    via: 'web'
  },
  {
    _id: '1a227508-9f39-427c-8f57-1b72f3fab87c',
    url: 'http://initech.tokoin.io.com/api/v2/tickets/1a227508-9f39-427c-8f57-1b72f3fab87c.json',
    external_id: '3e5ca820-cd1f-4a02-a18f-11b18e7bb49a',
    created_at: '2016-04-14T08:32:31 -10:00',
    type: 'incident',
    subject: 'A Catastrophe in Micronesia',
    description: 'Aliquip excepteur fugiat ex minim ea aute eu labore. Sunt eiusmod esse eu non commodo est veniam consequat.',
    priority: 'low',
    status: 'hold',
    submitter_id: 71,
    assignee_id: 38,
    organization_id: 112,
    tags: [
      'Puerto Rico',
      'Idaho',
      'Oklahoma',
      'Louisiana'
    ],
    has_incidents: false,
    due_at: '2016-08-15T05:37:32 -10:00',
    via: 'chat'
  },
  {
    _id: '2217c7dc-7371-4401-8738-0a8a8aedc08d',
    url: 'http://initech.tokoin.io.com/api/v2/tickets/2217c7dc-7371-4401-8738-0a8a8aedc08d.json',
    external_id: '3db2c1e6-559d-4015-b7a4-6248464a6bf0',
    created_at: '2016-07-16T12:05:12 -10:00',
    type: 'problem',
    subject: 'A Catastrophe in Hungary',
    description: 'Ipsum fugiat voluptate reprehenderit cupidatat aliqua dolore consequat. Consequat ullamco minim laboris veniam ea id laborum et eiusmod excepteur sint laborum dolore qui.',
    priority: 'normal',
    status: 'closed',
    submitter_id: 9,
    assignee_id: 65,
    organization_id: 105,
    tags: [
      'Massachusetts',
      'New York',
      'Minnesota',
      'New Jersey'
    ],
    has_incidents: true,
    due_at: '2016-08-06T04:16:06 -10:00',
    via: 'web'
  },
  {
    _id: '87db32c5-76a3-4069-954c-7d59c6c21de0',
    url: 'http://initech.tokoin.io.com/api/v2/tickets/87db32c5-76a3-4069-954c-7d59c6c21de0.json',
    external_id: '1c61056c-a5ad-478a-9fd6-38889c3cd728',
    created_at: '2016-07-06T11:16:50 -10:00',
    type: 'problem',
    subject: 'A Problem in Morocco',
    description: 'Sit culpa non magna anim. Ea velit qui nostrud eiusmod laboris dolor adipisicing quis deserunt elit amet.',
    priority: 'urgent',
    status: 'solved',
    submitter_id: 14,
    assignee_id: 7,
    organization_id: 118,
    tags: [
      'Texas',
      'Nevada',
      'Oregon',
      'Arizona'
    ],
    has_incidents: true,
    due_at: '2016-08-19T07:40:17 -10:00',
    via: 'voice'
  }
]

export const mockOrganizations = [
  {
    _id: 101,
    url: 'http://initech.tokoin.io.com/api/v2/organizations/101.json',
    external_id: '9270ed79-35eb-4a38-a46f-35725197ea8d',
    name: 'Enthaze',
    domain_names: [
      'kage.com',
      'ecratic.com',
      'endipin.com',
      'zentix.com'
    ],
    created_at: '2016-05-21T11:10:28 -10:00',
    details: 'MegaCorp',
    shared_tickets: false,
    tags: [
      'Fulton',
      'West',
      'Rodriguez',
      'Farley'
    ]
  },
  {
    _id: 102,
    url: 'http://initech.tokoin.io.com/api/v2/organizations/102.json',
    external_id: '7cd6b8d4-2999-4ff2-8cfd-44d05b449226',
    name: 'Nutralab',
    domain_names: [
      'trollery.com',
      'datagen.com',
      'bluegrain.com',
      'dadabase.com'
    ],
    created_at: '2016-04-07T08:21:44 -10:00',
    details: 'Non profit',
    shared_tickets: false,
    tags: [
      'Cherry',
      'Collier',
      'Fuentes',
      'Trevino'
    ]
  },
  {
    _id: 103,
    url: 'http://initech.tokoin.io.com/api/v2/organizations/103.json',
    external_id: 'e73240f3-8ecf-411d-ad0d-80ca8a84053d',
    name: 'Plasmos',
    domain_names: [
      'comvex.com',
      'automon.com',
      'verbus.com',
      'gogol.com'
    ],
    created_at: '2016-05-28T04:40:37 -10:00',
    details: 'Non profit',
    shared_tickets: false,
    tags: [
      'Parrish',
      'Lindsay',
      'Armstrong',
      'Vaughn'
    ]
  },
  {
    _id: 104,
    url: 'http://initech.tokoin.io.com/api/v2/organizations/104.json',
    external_id: 'f6eb60ad-fe37-4a45-9689-b32031399f93',
    name: 'Xylar',
    domain_names: [
      'anixang.com',
      'exovent.com',
      'photobin.com',
      'marqet.com'
    ],
    created_at: '2016-03-21T10:11:18 -11:00',
    details: 'MegaCörp',
    shared_tickets: false,
    tags: [
      'Hendricks',
      'Mclaughlin',
      'Stephens',
      'Garner'
    ]
  },
  {
    _id: 105,
    url: 'http://initech.tokoin.io.com/api/v2/organizations/105.json',
    external_id: '52f12203-6112-4fb9-aadc-70a6c816d605',
    name: 'Koffee',
    domain_names: [
      'farmage.com',
      'extrawear.com',
      'bulljuice.com',
      'enaut.com'
    ],
    created_at: '2016-06-06T02:50:27 -10:00',
    details: 'MegaCorp',
    shared_tickets: false,
    tags: [
      'Jordan',
      'Roy',
      'Mckinney',
      'Frost'
    ]
  }
]

export const mockSuggestions = [
  {
    matchedPropValue: 'jonibarlow@flotonic.com',
    matchedProp: 'email',
    id: 2
  },
  {
    matchedPropValue: 'Josefa Mcfadden',
    matchedProp: 'name',
    id: 9
  },
  {
    matchedPropValue: 'martinajoseph@flotonic.com',
    matchedProp: 'email',
    id: 14
  },
  {
    matchedPropValue: 'joneswright@flotonic.com',
    matchedProp: 'email',
    id: 26
  },
  {
    matchedPropValue: 'Munjor',
    matchedProp: 'tags',
    id: 34
  },
  {
    matchedPropValue: 'Johnsonburg',
    matchedProp: 'tags',
    id: 42
  },
  {
    matchedPropValue: 'Coinjock',
    matchedProp: 'tags',
    id: 43
  },
  {
    matchedPropValue: 'John Floyd',
    matchedProp: 'name',
    id: 44
  },
  {
    matchedPropValue: 'jodyholcomb@flotonic.com',
    matchedProp: 'email',
    id: 49
  }
]

export const mockUpdateUser = {
  _id: 1,
  url: 'http://initech.tokoin.io.com/api/v2/users/1.json',
  external_id: '74341f74-9c79-49d5-9611-87ef9b6eb75f',
  name: 'New Updated',
  alias: 'Mock Alias',
  created_at: '2016-04-15T05:19:46 -10:00',
  active: true,
  verified: true,
  shared: false,
  locale: 'en-AU',
  timezone: 'Sri Lanka',
  last_login_at: '2013-08-04T01:03:27 -10:00',
  email: 'test@mock.com',
  phone: '1111-111-1111',
  signature: 'New Mock Signature',
  organization_id: 119,
  tags: [
    'Springville',
    'Sutton',
    'Hartsville/Hartley',
    'Diaperville',
    'Henrietta'
  ],
  suspended: true,
  role: 'admin'
}

export const mockUpdateTicket = {
  _id: '436bf9b0-1147-4c0a-8439-6f79833bff5b',
  url: 'http://initech.tokoin.io.com/api/v2/tickets/436bf9b0-1147-4c0a-8439-6f79833bff5b.json',
  external_id: '9210cdc9-4bee-485f-a078-35396cd74063',
  created_at: '2016-04-28T11:19:34 -10:00',
  type: 'problem',
  subject: 'The updated problem',
  description: 'Nostrud ad sit',
  priority: 'low',
  status: 'pending',
  submitter_id: 38,
  assignee_id: 24,
  organization_id: 116,
  tags: [
    'Ohio',
    'Pennsylvania',
    'American Samoa',
    'Northern Mariana Islands'
  ],
  has_incidents: false,
  due_at: '2016-07-31T02:37:50 -10:00',
  via: 'web'
}

export const mockUpdatedOrganization = {
  _id: 101,
  url: 'http://initech.tokoin.io.com/api/v2/organizations/101.json',
  external_id: '9270ed79-35eb-4a38-a46f-35725197ea8d',
  name: 'Sample',
  domain_names: [
    'kage.com',
    'ecratic.com',
    'endipin.com',
    'zentix.com'
  ],
  created_at: '2016-05-21T11:10:28 -10:00',
  details: 'For profit',
  shared_tickets: false,
  tags: [
    'Fulton',
    'West',
    'Rodriguez',
    'Farley'
  ]
}
